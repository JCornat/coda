CREATE DATABASE  IF NOT EXISTS `coda` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `coda`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: coda
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `background` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'Virtual Riot','http://image.noelshack.com/fichiers/2015/02/1420489339-vr.jpg','http://image.noelshack.com/fichiers/2015/02/1420489340-vr1.jpg'),(2,'Cash Cash','http://image.noelshack.com/fichiers/2015/02/1420489337-cc.jpg','http://image.noelshack.com/fichiers/2015/02/1420489338-cash-cash.jpg'),(3,'3LAU','http://image.noelshack.com/fichiers/2015/02/1420489336-3lau1.jpg','http://image.noelshack.com/fichiers/2015/02/1420489338-3lau.jpg'),(4,'Nicky Romero','http://image.noelshack.com/fichiers/2015/02/1420489337-nr.jpg','http://image.noelshack.com/fichiers/2015/02/1420489340-nr1.jpg'),(5,'Matthew Koma','http://image.noelshack.com/fichiers/2015/02/1420489336-mk.jpg','http://image.noelshack.com/fichiers/2015/02/1420489338-mk1.jpg'),(6,'ZHU','http://image.noelshack.com/fichiers/2015/02/1420489339-zhu.jpg','http://image.noelshack.com/fichiers/2015/02/1420489340-zhu1.jpg');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `track`
--

DROP TABLE IF EXISTS `track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `played` int(11) NOT NULL,
  `id_author` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`played`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `track`
--

LOCK TABLES `track` WRITE;
/*!40000 ALTER TABLE `track` DISABLE KEYS */;
INSERT INTO `track` VALUES (1,'Idols',0,1,'http://cornat-development.com/musics/d95fb2a2046c8c51a30fb3e1187d592f.mp3','http://image.noelshack.com/fichiers/2015/02/1420490304-idols.jpg'),(2,'Surrender',0,2,'http://cornat-development.com/musics/ce06362d5b0ef81217754d7ce7d20f38.mp3','http://image.noelshack.com/fichiers/2015/02/1420490305-surrender.jpg'),(3,'Take Me Home',0,2,'http://cornat-development.com/musics/a3a16169c52d57604b99be3e4da44c10.mp3','http://image.noelshack.com/fichiers/2015/02/1420490305-takemehome.jpg'),(4,'How You Love Me',0,3,'http://cornat-development.com/musics/a290ce2882ccfe891e481e1d2804badb.mp3','http://image.noelshack.com/fichiers/2015/02/1420490304-howyouloveme.jpg'),(5,'Overtime',0,2,'http://cornat-development.com/musics/cb2ee3b0baf784d05dc3eba64e5451b3.mp3','http://image.noelshack.com/fichiers/2015/02/1420490304-overtim.jpg'),(6,'Sparks',0,4,'http://cornat-development.com/musics/19c5b842d4e4ddbfeeb941bcd070ac48.mp3','http://image.noelshack.com/fichiers/2015/02/1420490305-saprks.jpg'),(7,'One Night',0,5,'http://cornat-development.com/musics/411ed7d4020acb2bcaddabf82de36f29.mp3','http://image.noelshack.com/fichiers/2015/02/1420490305-onenight.jpg'),(8,'Faded',0,6,'http://cornat-development.com/musics/92d26682231637dc893928182c75500f.mp3','http://image.noelshack.com/fichiers/2015/02/1420490304-faded.jpg');
/*!40000 ALTER TABLE `track` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-05 21:52:07
