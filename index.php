<?php

require "vendor/autoload.php";

$app = new \Slim\Slim();

$app->group('/api', function() use ($app) {
    $app->group('/track', function() use ($app) {
        $app->get('/:id', function($id) {
            \config\Connexion::connection("config.ini");
            echo \model\Track::with('author')->get()->find($id)->toJson();
        });
    });
    $app->group('/album', function() use ($app) {
        $app->get('/:id', function($id) {
            \config\Connexion::connection("config.ini");
            echo \model\Album::find($id)->toJson();
        });
    });
    $app->group('/author', function() use ($app) {
        $app->get('/:id', function($id) {
            \config\Connexion::connection("config.ini");
            echo \model\Author::find($id)->toJson();
        });
    });
});

$app->get('/authors', function() use ($app) {
    \config\Connexion::connection("config.ini");
    $data = \model\Author::all();

    $view = new view\Authors();
    $view->addVar('authors', $data);
    if ($app->request->isAjax()) {
        echo $view->render(true);
    } else {
        echo $view->render();
    }
});

$app->get('/playlist', function() use ($app) {
    $view = new view\Playlist();
    if ($app->request->isAjax()) {
        echo $view->render(true);
    } else {
        echo $view->render();
    }
});

$app->get('/author/:id', function($id = null) use ($app) {
    \config\Connexion::connection("config.ini");
    $tracks = \model\Track::where("id_author", "=", $id)->get();
    $author = \model\Author::find($id);

    $view = new view\Detail();
    $view->addVar('tracks', $tracks);
    $view->addVar('author', $author);
    if ($app->request->isAjax()) {
        echo $view->render(true);
    } else {
        echo $view->render();
    }
});

$app->get('/search', function() use ($app) {
    $view = new view\Search();
    if(isset($_GET['s'])) {
        $search = htmlspecialchars($_GET['s']);
        \config\Connexion::connection("config.ini");
        $data['author'] = \model\Author::where('name', 'LIKE', '%'.$search.'%')->get();
        $data['track'] = \model\Track::where('title', 'LIKE', '%'.$search.'%')->with('author')->get();

        $view->addVar('search',$search);
        if(isset($_GET['f'])) {
            $filter = htmlspecialchars($_GET['f']);
            $view->addVar('filter', $filter);
        }
        $view->addVar('results', $data);
    }

    if ($app->request->isAjax()) {
        echo $view->render(true);
    } else {
        echo $view->render();
    }
});

$app->get('/', function() use ($app) {
    $view = new view\Home();
    if ($app->request->isAjax()) {
        echo $view->render(true);
    } else {
        echo $view->render();
    }
});

$app->get('/tracks', function() use ($app) {
    \config\Connexion::connection("config.ini");
    $data = \model\Track::with('author')->get();

    $view = new view\Music();
    $view->addVar('tracks', $data);
    if ($app->request->isAjax()) {
        echo $view->render(true);
    } else {
        echo $view->render();
    }
});


$app->run();