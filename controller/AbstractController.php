<?php namespace controller;

abstract class AbstractController {
    public abstract function __construct();
} 