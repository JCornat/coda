<?php

namespace view;

class Search extends View {

    public function __construct() {
        $this->layout = "search.html.twig";
        $this->layoutAjax = "search.twig";
    }

}