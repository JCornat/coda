<?php

namespace view;

abstract class View {

    protected $layout;
    protected $layoutAjax;
    protected $data = [];
    protected $obj;

    public function __construct($o = null) {
        $this->obj = $o;
    }

    public function addVar($key, $value) {
        $this->data[$key] = $value;
    }

    public function render($ajax = null) {
        $loader = new \Twig_Loader_Filesystem('templates');
        $twig = new \Twig_Environment($loader, array());
        $template = (isset($ajax) ? $twig->loadTemplate($this->layoutAjax) : $twig->loadTemplate($this->layout));
        return $template->render($this->data);
    }
} 