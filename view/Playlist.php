<?php

namespace view;

class Playlist extends View {

    public function __construct() {
        $this->layout = "playlist.html.twig";
        $this->layoutAjax = "playlist.twig";
    }

}