<?php

namespace view;

class Authors extends View {

    public function __construct() {
        $this->layout = "authors.html.twig";
        $this->layoutAjax = "authors.twig";
    }

}