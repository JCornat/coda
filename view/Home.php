<?php

namespace view;

class Home extends View {

    public function __construct() {
        $this->layout = "home.html.twig";
        $this->layoutAjax = "home.twig";
    }

}