<?php

namespace view;

class Music extends View {

    public function __construct() {
        $this->layout = "tracks.html.twig";
        $this->layoutAjax = "tracks.twig";
    }

}