<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Track extends Model{

    public $table = 'track';
    public $idTable = 'id';
    public $timestamps = false;

    public function author() {
        return $this->belongsTo('model\Author', 'id_author');
    }
}