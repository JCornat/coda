<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Album extends Model{

    public $table = 'album';
    public $idTable = 'id';
    public $timestamps = false;

    public function author() {
        return $this->belongsTo('models\Author', 'id_author');
    }
}