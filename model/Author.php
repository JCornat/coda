<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Author extends Model{

    public $table = 'author';
    public $idTable = 'id';
    public $timestamps = false;

//    public function author() {
//        return $this->belongsTo('model\Author', 'id_author');
//    }
}