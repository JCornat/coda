$ ->

    $(document).on 'click', 'a', (e) ->
        href = $(this).attr('href')
        if(href.charAt(0) == "/")
            e.preventDefault()
            e.stopPropagation()
            window.history.pushState({page: 1}, document.title, href);
            ajaxQuery(href)

    $(document).on 'submit', 'form', (e) ->
        e.preventDefault()
        e.stopPropagation()
        href = $(this).attr('action')+"?"+$(this).serialize()
        window.history.pushState({page: 1}, document.title, href);
        ajaxQuery(href)

    ajaxQuery = (url) ->
        $.ajax url,
            type: 'GET'
            success: (html) ->
                $("#browser").empty().append(html)
            error: ->
                console.log("ERROR AJAX")

    window.onpopstate = (e) ->
        ajaxQuery(document.location.href)
