$ ->

    $player = $('#player')
    $audioPlayer = $('#audioPlayer')

    clicked = (e) ->
        e.stopPropagation()
        if !musicManager.isPlaying()
            $player.find('i.fa-play').removeClass('fa-play').addClass('fa-pause')
            musicManager.setResume()
        else
            $player.find('i.fa-pause').removeClass('fa-pause').addClass('fa-play')
            musicManager.pause()

    updateVolumeBar = ->
        $player.find('.volume').val(Math.round(musicManager.getVolume() * 100))

    window.updateVolumeBar = updateVolumeBar

    updateTrackVolume = (e) ->
        musicManager.setVolume($(this).val()/100)

    updateTrackVolumeOff = (e) ->
        musicManager.setVolume(0)
        updateVolumeBar()

    updateTrackVolumeUp = (e) ->
        musicManager.setVolume(musicManager.getVolume()+0.1)
        updateVolumeBar()

    updateIconPlayer = (bool) ->
        if bool
            $player.find('i.fa-play').removeClass('fa-play').addClass('fa-pause')
        else
            $player.find('i.fa-pause').removeClass('fa-pause').addClass('fa-play')

    nextTrack = ->
        if musicManager.currentTrack
            count = musicManager.playlist.length
            i = 0
            pos = 0
            for track in musicManager.playlist
                i++
                if musicManager.currentTrack.id == track.id
                    pos = i
            if pos >= count
                pos = 0
            music = musicManager.playlist[pos]
            musicManager.play(music)
            checkPlay();

    previousTrack = ->
        if musicManager.currentTrack
            count = musicManager.playlist.length
            pos = $.inArray(musicManager.currentTrack, musicManager.playlist) - 1
            if pos < 0
                pos = count-1
            music = musicManager.playlist[pos]
            if musicManager.element.currentTime > 5
                musicManager.setCurrentTime(0)
            else
                musicManager.play(music)
                checkPlay();

    $audioPlayer[0].onplay = -> updateIconPlayer(true)
    $audioPlayer[0].onpause = -> updateIconPlayer(false)
    $audioPlayer[0].onended = -> nextTrack()

    seekingProgress = (e) ->
        posX = e.clientX - $('.track-progress').offset().left
        percent = posX / $('.track-progress').width()
        musicManager.setCurrentTime(percent)

    numberToMinuteSecond = (number) ->
        min = Math.floor(number/60)
        sec = "0"+Math.round(((number%60)*100)/100)
        sec = sec.slice(-2)
        return {"0":min, "1":sec}

    updateProgressBar = ->
        if musicManager.currentTrack != null
            percent = (musicManager.getCurrentTime()/musicManager.getDuration())
            calc = $player.find('.track-progress').width() - percent*$('#player .track-progress').width()
            $player.find('.track-progress-bar').css("right", calc+'px')

        if musicManager.currentTrack != null && musicManager.getCurrentTime() != NaN && musicManager.getDuration() != NaN
            currentTime = numberToMinuteSecond(musicManager.getCurrentTime())
            duration = numberToMinuteSecond(musicManager.getDuration())
            $player.find('span.currentTime').html(currentTime[0]+ ":" + currentTime[1])
            $player.find('span.duration').html(duration[0]+ ":" + duration[1])

    $player
        .on 'click', '.play-pause', clicked
        .on 'click', '.track-progress', seekingProgress
        .on 'change', '.volume', updateTrackVolume
        .on 'click', '.volume-off', updateTrackVolumeOff
        .on 'click', '.volume-up', updateTrackVolumeUp
        .on 'click', '.previousTrack', previousTrack
        .on 'click', '.nextTrack', nextTrack

    setInterval(updateProgressBar, 500)

    class Player

        #Update data on the player
        updateData: ->
            $player.find('.player-track .cover').css({
                'background-image':'url('+musicManager.currentTrack.cover+')'
            });
            $player.find('.track-data .title').html(musicManager.currentTrack.title)

            # Retrieve author data
            $.get '/coda/api/author/'+musicManager.currentTrack.id_author, (data) ->
                parsed = $.parseJSON(data)
                $player.find('.track-data .author').html('<a href="/coda/author/'+musicManager.currentTrack.id_author+'">'+parsed.name+'</a>')

    window.Player = Player
