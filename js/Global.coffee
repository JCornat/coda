$ ->
    checkPlay = ->
        $('#browser').find('i.fa-pause').removeClass('fa-pause').addClass('fa-play')
        if (typeof musicManager != 'undefined' && musicManager.currentTrack)
            $('#browser #'+musicManager.currentTrack.id).find('i.fa-play').removeClass('fa-play').addClass('fa-pause')
        else
            setTimeout(checkPlay, 500)

    checkList = ->
        $('#browser i.fa-check').removeClass('fa-check').addClass('fa-plus')
        if (typeof musicManager != 'undefined' && musicManager.playlist)
            for music in musicManager.playlist
                $('#browser #'+music.id+' i.fa-plus').removeClass('fa-plus').addClass('fa-check')
        else
            setTimeout(checkList, 500)

    window.checkPlay = checkPlay;
    window.checkList = checkList;