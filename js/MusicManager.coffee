$ ->
    class MusicManager
        playing: false
        currentTrack: null
        playlist: []

        constructor: (element) ->
            @element = element[0]
            @element.volume = 0.2

        play: (music) ->
            if (@currentTrack == null)
                @currentTrack = music
                @element.src = music.url
                @setPlay()
                return true
            if (@currentTrack && music != @currentTrack)
                @currentTrack = music
                @element.src = music.url
                @setPlay()
                return true
            @setPlay(false)
            return true

        pause: ->
            if (@playing == false)
                return false
            @setPause()
            return true

        isPlaying: ->
            @playing

        setVolume: (number) ->
            if(number > 1)
                @element.volume = 1
            else
                @element.volume = number

        getVolume: ->
            @element.volume

        getCurrentTime: ->
            @element.currentTime

        setCurrentTime: (percent) ->
            if(@currentTime != null)
                @element.currentTime = @element.duration * percent

        getDuration: ->
            @element.duration

        setPlay: (boole = true) ->
            @element.play()
            @playing = true
            if boole
                player.updateData()

        setResume: ->
            @element.play()
            @playing = true

        setPause: ->
            @element.pause()
            @playing = false

        addTrack: (music) ->
            ok = true
            for mus in @playlist
                if mus.id == music.id
                    ok = false
            if ok
                @playlist.push(music)
            if !@currentTrack
                @play(music)
            ok

    window.MusicManager = MusicManager
