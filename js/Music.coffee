class Music
    constructor: (id, title, url, cover, id_author, author_name) ->
        @id = id
        @title = title
        @url = url
        @cover = cover
        @id_author = id_author
        @author_name = author_name
        @duration = null

window.Music = Music