// Generated by CoffeeScript 1.8.0
(function() {
  $(function() {
    var MusicManager;
    MusicManager = (function() {
      MusicManager.prototype.playing = false;

      MusicManager.prototype.currentTrack = null;

      MusicManager.prototype.playlist = [];

      function MusicManager(element) {
        this.element = element[0];
        this.element.volume = 0.2;
      }

      MusicManager.prototype.play = function(music) {
        if (this.currentTrack === null) {
          this.currentTrack = music;
          this.element.src = music.url;
          this.setPlay();
          return true;
        }
        if (this.currentTrack && music !== this.currentTrack) {
          this.currentTrack = music;
          this.element.src = music.url;
          this.setPlay();
          return true;
        }
        this.setPlay(false);
        return true;
      };

      MusicManager.prototype.pause = function() {
        if (this.playing === false) {
          return false;
        }
        this.setPause();
        return true;
      };

      MusicManager.prototype.isPlaying = function() {
        return this.playing;
      };

      MusicManager.prototype.setVolume = function(number) {
        if (number > 1) {
          return this.element.volume = 1;
        } else {
          return this.element.volume = number;
        }
      };

      MusicManager.prototype.getVolume = function() {
        return this.element.volume;
      };

      MusicManager.prototype.getCurrentTime = function() {
        return this.element.currentTime;
      };

      MusicManager.prototype.setCurrentTime = function(percent) {
        if (this.currentTime !== null) {
          return this.element.currentTime = this.element.duration * percent;
        }
      };

      MusicManager.prototype.getDuration = function() {
        return this.element.duration;
      };

      MusicManager.prototype.setPlay = function(boole) {
        if (boole == null) {
          boole = true;
        }
        this.element.play();
        this.playing = true;
        if (boole) {
          return player.updateData();
        }
      };

      MusicManager.prototype.setResume = function() {
        this.element.play();
        return this.playing = true;
      };

      MusicManager.prototype.setPause = function() {
        this.element.pause();
        return this.playing = false;
      };

      MusicManager.prototype.addTrack = function(music) {
        var mus, ok, _i, _len, _ref;
        ok = true;
        _ref = this.playlist;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          mus = _ref[_i];
          if (mus.id === music.id) {
            ok = false;
          }
        }
        if (ok) {
          this.playlist.push(music);
        }
        if (!this.currentTrack) {
          this.play(music);
        }
        return ok;
      };

      return MusicManager;

    })();
    return window.MusicManager = MusicManager;
  });

}).call(this);
