$ ->
    window.player = new Player
    window.musicManager = new MusicManager $('#audioPlayer')

    if (window.localStorage.getItem('volume'))
        updateVolumeBar()

    clicked = (event) ->
        event.preventDefault()
        event.stopPropagation()
        media = $(this).closest('.track-thumbnail')
        $.get '/coda/api/track/'+media.attr('id'), (data) ->
            parsed = $.parseJSON(data)
            music = new Music(parsed.id, parsed.title, parsed.url, parsed.cover, parsed.id_author, parsed.author.name)
            musicManager.playlist = [music]
            musicManager.play(music)
            checkPlay()
            checkList()

    addToPlaylist = (event) ->
        event.preventDefault()
        event.stopPropagation()
        el = $(this)
        media = el.closest('.track-thumbnail')
        $.get '/coda/api/track/'+media.attr('id'), (data) ->
            parsed = $.parseJSON(data)
            tmp = new Music(parsed.id, parsed.title, parsed.url, parsed.cover, parsed.id_author, parsed.author.name)
            if musicManager.addTrack(tmp)
                el.find('i.fa-plus').removeClass('fa-plus').addClass('fa-check')

    window.updatePlaylist = ->
        $ul = $('.playlist-list tbody')
        $ul.sortable
            update: ->
                musicManager.playlist = []
                $ul.find('tr').each ->
                    m = new Music $(this).attr('data-track-id'), $(this).attr('data-track-title'), $(this).attr('data-track-url'), $(this).attr('data-track-cover'), $(this).attr('data-author-id'), $(this).attr('data-author-name')
                    musicManager.addTrack(m)

        $ul.disableSelection()
        if(musicManager.playlist.length > 0)
            $ul.html('')
        for music in musicManager.playlist
            $ul.append('<tr id="'+music.id+'" data-track-id="'+music.id+'" data-track-title="'+music.title+'" data-track-url="'+music.url+'" data-track-cover="'+music.cover+'" data-author-id="'+music.id_author+'" data-author-name="'+music.author_name+'"><td class="status"><i class="fa fa-play"></i></td><td class="title">'+music.title+'</td><td class="author_name">'+music.author_name+'</td></tr>')

    #Add click listener on play-pause buttons
    $(document).on('click', '.play-pause', clicked)

    #Add click listener on add to playlist buttons
    $(document).on('click', '.add-to-playlist', addToPlaylist)

    #Remove every playing icon and playing class in browser
    removeAllPlaying = ->
        $('#browser').find('i.fa-pause').each( -> $(this).removeClass('fa-pause').addClass('fa-play')).end().find('div.play-pause').each( -> $(this).removeClass('playing'))

    #Remove playing icon and playing class in one element
    removePlaying = (element) ->
        element.removeClass('playing').find('i.fa-pause').removeClass('fa-pause').addClass('fa-play')

    #Add playing icon and playing class in one element
    addPlaying = (element) ->
        element.addClass('playing').find('i.fa-play').removeClass('fa-play').addClass('fa-pause')

